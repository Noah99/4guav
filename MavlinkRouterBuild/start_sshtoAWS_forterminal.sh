#!/bin/bash

set -e
set -x


autossh -N -R 6000:localhost:22 -i "/home/uas/AWS_UAS.pem" ubuntu@AWSIPADDRESS
