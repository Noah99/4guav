#!/bin/bash

# Copyright Peter Burke 12/4/2018

# define functions first


function installstuff {
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Installing a whole bunch of packages..."

    start_time_installstuff="$(date -u +%s)"

    time sudo apt-get -y update # 1 min   #Update the list of packages in the software center                                   
    time sudo apt-get -y upgrade # 3.5 min
    time sudo apt-get -y install screen # 0.5 min
    time sudo apt-get -y install tcptrack # 0 min
    time sudo apt-get -y install python  # 6 sec
    time sudo apt-get -y install python-wxgtk2.8 # 4 min 
    time sudo apt-get -y install python-matplotlib # 
    time sudo apt-get -y install python-opencv # 2 min
    time sudo apt-get -y install python-pip # 3 min
    time sudo apt-get -y install python-numpy  # 0 min
    time sudo apt-get -y install python-dev # 0 min
    time sudo apt-get -y install libxml2-dev # 1 min
    time sudo apt-get -y install libxslt-dev # 0.5 min
    time sudo apt-get -y install python-lxml # 0.75 min
    time sudo apt-get -y install python-setuptools # 0 min
    time sudo apt-get -y install git # 1 min
    time sudo apt-get -y install dh-autoreconf # 1 min
    time sudo apt-get -y install systemd # 0 min
    time sudo apt-get -y install wget # 0 min
    time sudo apt-get -y install gcc            #ADDED 6/17/2019
    time sudo apt-get -y install emacs # 4.5 min  (seems you have to run this twice)                                             
    time sudo apt-get -y install emacs # 0 min (seems you have to run this twice)                                             
    time sudo apt-get -y install nload # 0.5 min (network monitor, launch with nload -m)                                         
    time sudo apt-get -y install build-essential # 0 min
    time sudo apt-get -y install autossh # 0.5 min
    time sudo apt-get -y install python3-pip        #ADDED 6/17/2019

    echo "Done installing a whole bunch of packages..."

    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "pip installing future, pymavlink, mavproxy..."
    time sudo pip install future # 1 min
    time python3 -m pip install future          #ADDED 6/17/2019
    time sudo pip install pymavlink # 5.5 min
    time sudo pip install mavproxy # 4 min
    echo "Done pip installing future, pymavlink, mavproxy..."


    end_time_installstuff="$(date -u +%s)"
    elapsed_installstuff="$(($end_time_installstuff-$start_time_installstuff))"
    echo "Total of $elapsed_installstuff seconds elapsed for installing packages"
    # 38 mins
    
    
}

function downloadandbuildmavlinkrouter {

    start_time_downloadandbuildmavlinkrouter="$(date -u +%s)"
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Downloading git clone of mavlink-router..."
    #Download the git clone:                                                                                        

    git clone https://github.com/intel/mavlink-router.git
    cd mavlink-router
    sudo git submodule update --init --recursive

    echo "Done downloading git clone of mavlink-router..."

    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Start making / compiling / building mavlink-router..."

    sudo ./autogen.sh && sudo ./configure CFLAGS='-g -O2' --sysconfdir=/etc --localstatedir=/var --libdir=/usr/lib64 --prefix=/usr #--disable-systemd # needed for AWS linux                                                       

    #Make it                                                                                                        

    sudo make
    
    sudo make install           #ADDED 6/17/2019

    echo "Done making / compiling / building / installing mavlink-router..."

    end_time_downloadandbuildmavlinkrouter="$(date -u +%s)"
    elapsed_downloadandbuildmavlinkrouter="$(($end_time_downloadandbuildmavlinkrouter-$start_time_downloadandbuildmavlinkrouter))"
    echo "Total of $elapsed_downloadandbuildmavlinkrouter seconds elapsed for downloading and building mavlink router"
    # 13 min

}

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}

#***********************END OF FUNCTION DEFINITIONS******************************

set -x

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will install mavlink router and set it up for you."
echo "See README in 4guav gitlab repo for documentation."

read -p "To begin with the installation type in the EXACT IP address of the AWS instance for the ssh tunnel: " awsip

echo "You entered:"
echo $awsip

read -p "If this is correct, enter "yes": " out

if ! [ "$out" = "yes" ]
then
  echo "You did not type in 'yes'. Exiting... Nothing has been done."
  exit 1
fi

echo "Starting..."

date


start_time="$(date -u +%s)"

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

echo "Disabling bluetooth using config.txt so UART works"
echo "Appending to /boot/config.txt..."

# If there is already a line delete it.                               
 sudo sed -i '/dtoverlay=pi3-disable-bt/d'  /boot/config.txt

 a="
 dtoverlay=pi3-disable-bt
 "
 sudo sh -c "echo '$a'>>/boot/config.txt"

# Now clean up extra spaced lines:
tmpfile=$(mktemp)
sudo awk '!NF {if (++n <= 1) print; next}; {n=0;print}' /boot/config.txt > "$tmpfile" && sudo mv "$tmpfile" /boot/config.txt

check_errors
echo "Appended to /boot/config.txt"

installstuff

downloadandbuildmavlinkrouter
                                                            


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Start configuring mavlink-router..."
echo "It will connect to flight controller on /dev/AMA0"
echo "It will serve up a mavlink stream on localhost port 5678 TCP"


#Configure it                                                                                                   

if [ ! -d "/etc/mavlink-router" ] 
then
    echo "Directory /etc/mavlink-router does not exist yet. Making it." 
    sudo mkdir /etc/mavlink-router
    echo "Made /etc/mavlink-router" 
fi

cd /etc/mavlink-router
# wget main.conf #  for mavlink-router configuration
wget https://gitlab.com/Noah99/4guav/raw/master/MavlinkRouterBuild/main.conf -O /etc/mavlink-router/main.conf
sudo chmod 777 main.conf
echo "Done configuring mavlink-router..."


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Now download the autostart scripts for mavlink-router"
if [ ! -d "/home/pi/startupscripts" ] 
then
    echo "Directory /home/pi/startupscripts does not exist yet. Making it." 
    sudo -u pi mkdir /home/pi/startupscripts
    echo "Made /home/pi/startupscripts" 
fi
cd /home/pi/startupscripts
wget https://gitlab.com/Noah99/4guav/raw/master/MavlinkRouterBuild/autostart_mavlinkrouter.sh -O /home/pi/startupscripts/autostart_mavlinkrouter.sh
wget https://gitlab.com/Noah99/4guav/raw/master/MavlinkRouterBuild/start_mavlinkrouter.sh  -O /home/pi/startupscripts/start_mavlinkrouter.sh
echo "Did download the autostart scripts for mavlink-router"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "adding autostart_mavlinkrouter.sh to /etc/rc.local"

# If line already was added delete it. Note this is overkill could delete it manually if you wanted.
sed -i -n '/sleep 45/!p' /etc/rc.local

sed -i -n '/autostart_mavlinkrouter/!p' /etc/rc.local


# Add these lines to /etc/rc.local
#sudo -H -u pi /bin/bash -c '/home/pi/startupscripts/autostart_mavlinkrouter.sh'
a="
sleep 45
sudo -H -u pi /bin/bash -c '/home/pi/startupscripts/autostart_mavlinkrouter.sh'
exit 0"
# delete exit on last line of /etc/rc.local
sudo sed -i '/exit 0/d'  /etc/rc.local
# append a to end
sudo sh -c "echo '$a'>>/etc/rc.local"
sudo chown root:root /etc/rc.local
sudo chmod 755 /etc/rc.local
echo "added autostart_mavlinkrouter.sh to /etc/rc.local"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Now download the autostart scripts for ssh to AWS"
if [ ! -d "/home/pi/startupscripts" ] 
then
    echo "Directory /home/pi/startupscripts does not exist yet. Making it." 
    sudo -u pi mkdir /home/pi/startupscripts
    echo "Made /home/pi/startupscripts" 
fi
cd /home/pi/startupscripts
wget https://gitlab.com/Noah99/4guav/raw/master/MavlinkRouterBuild/autostart_sshtoAWS.sh -O /home/pi/startupscripts/autostart_sshtoAWS.sh 
wget https://gitlab.com/Noah99/4guav/raw/master/MavlinkRouterBuild/start_sshtoAWS.sh -O /home/pi/startupscripts/start_sshtoAWS.sh 


wget https://gitlab.com/Noah99/4guav/raw/master/MavlinkRouterBuild/autostart_sshtoAWS_forterminal.sh -O /home/pi/startupscripts/autostart_sshtoAWS_forterminal.sh 
wget https://gitlab.com/Noah99/4guav/raw/master/MavlinkRouterBuild/start_sshtoAWS_forterminal.sh -O /home/pi/startupscripts/start_sshtoAWS_forterminal.sh 

echo "Did download the autostart scripts for sshtoAWS"

sudo chmod 777 *.sh

echo "Editing sshtoAWS.sh to put IP address in that you entered above."

# awsip was entered above
sed -i "s/AWSIPADDRESS/$awsip/g" /home/pi/startupscripts/start_sshtoAWS.sh
sed -i "s/AWSIPADDRESS/$awsip/g" /home/pi/startupscripts/start_sshtoAWS_forterminal.sh



echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "adding autostart_sshtoAWS.sh to /etc/rc.local"
# Add these lines to /etc/rc.local
#sudo -H -u pi /bin/bash -c '/home/pi/startupscripts/autostart_mavlinkrouter.sh'
#sudo -H -u pi /bin/bash -c '/home/pi/startupscripts/autostart_sshtoAWS.sh'

# If line already was added delete it.
sed -i -n '/autostart_sshtoAWS/!p' /etc/rc.local
sed -i -n '/autostart_sshtoAWS_forterminal/!p' /etc/rc.local


a="
printf "************* starting rc.local ******************* \n"
# Print the IP address
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
  printf "My IP address is %s\n" "$_IP"
fi
printf "********** sleeping 25  ***************** \n"
sleep 25
printf "********** did sleep 25  ***************** \n"
# modem boots in 22 sec so this is fine
# if modem was booting during dhcpcd client startup, things dont work
# so restart dhcpcd to reget IP addresses from servers eg 4G modem server

dhcpcd -n # needed to get ip address again from 4G modem if it was booting

# Print the IP address
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
  printf "My IP address is %s\n" "$_IP"
fi

sudo -H -u pi /bin/bash -c '/home/pi/startupscripts/autostart_sshtoAWS.sh'
sudo -H -u pi /bin/bash -c '/home/pi/startupscripts/autostart_sshtoAWS_forterminal.sh'
exit 0
"
# delete exit on last line of /etc/rc.local
sudo sed -i '/exit 0/d'  /etc/rc.local
# append a to end
sudo sh -c "echo '$a'>>/etc/rc.local"
sudo chown root:root /etc/rc.local
sudo chmod 755 /etc/rc.local
echo "added autostart_sshtoAWS.sh to /etc/rc.local"

# Now clean up extra spaced lines:
tmpfile=$(mktemp)
sudo awk '!NF {if (++n <= 1) print; next}; {n=0;print}' /etc/rc.local > "$tmpfile" && sudo mv "$tmpfile" /etc/rc.local
sudo chmod 777 /etc/rc.local


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

date

end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for the entire process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."


