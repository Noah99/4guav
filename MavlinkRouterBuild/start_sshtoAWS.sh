#!/bin/bash

set -e
set -x

# start mavlink router hopefully /etc/mavlink-router/main.conf loads:

autossh -N -R 1234:localhost:5678 -i "/home/uas/AWS_UAS.pem" ubuntu@AWSIPADDRESS
