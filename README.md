## Overview

This repo is a set of scripts and instructions on how to get a 4g modem enable UAV connected to a ground control station computer.

The hardware on the UAV is a flight controller running Arudpilot software, and a Raspberry Pi Zero W with a 4g USB cellular modem.

The flight controller to Pi connection is over a UART on the Pi's serial pins.

![alt text](https://gitlab.com/pjbca/4guav/raw/master/Configuration.svg)

There are three main modules/packages:
* Mavlink-router
* Webcam
* Wifi access point to UAV

See the wiki for "under the hood" details and operational field manuals.

### Mavlink-router

This is the meat of the project, and connects the UAV to a ground computer over the internet.
It uses a cloud server to encrypt, stabilize, and coordinate all communications since a fixed IP on either end is not likely.
These instructions are for an Amazon Web Service (AWS) instance, but others should work as well.

### Webcam

An HD Pi camera can provide video, which a user on the ground can view on any browser.

### Wifi access point to UAV

For convenience, a method has been provided to connect to the AUV with the Pi's wifi.



## Target hardware/prerequisites

* SD Card
* Raspberry Pi Zero W
* 4g Modem (tested on Verizon USB730L)
* Ardupilot flight controller (tested on Omnibus F4 pro V2)
* Cloud based linux instance with public IP (I used an AWS instance)
* Win10 desktop with internet access

## Prerequisite knowledge

To implement this configuration, you should be familiar with Ardupilot and how it works with UAVs, as well as the ground control station software Mission Planner.

You should also have basic familiarity with how to operate in a linux environment, how to log into a linux machine remotely using ssh, how to copy and move files, and list files in a directory.


## Installing

Follow the instructions for each package, picking which ones you want.
There is a readme for each one in its sub-directory.
I recommend to start with the mavlink-router build then add the other two if you want them.


## How it works:

The UAV Pi "calls" the cloud AWS server on reverse ssh.
The Win10 ground station "calls" the AWS server on reverse ssh.

Thus, all communication is encrypted.

Both end points (UAV, Win10 ground control computer) do not need static IP addresses.


## Authors

* **Peter Burke** - *Initial work* - [GitLab](https://gitlab.com/pjbca)

## License

This project is licensed under the GNU License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

 * Ardupilot
